# Sexual Harassment

**1. What kinds of behaviour cause sexual harassment?**
 * There are three types of sexual harassment- Visual, Verbal and Physical.
 * Making conditions of employment or advancement dependent on sexual favors, either explicitly or implicitly.
 * Physical acts of sexual assault.
 * Requests for sexual favors.
 * Verbal harassment of a sexual nature, including jokes referring to sexual acts or sexual orientation.
 * Unwanted touching or physical contact.
 * Unwelcome sexual advances.
 * Discussing sexual relations/stories/fantasies at work, school, or in other inappropriate places.
 * Feeling pressured to engage with someone sexually.
 * Exposing oneself or performing sexual acts on oneself.
 * Unwanted sexually explicit photos, emails, or text messages.

**2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?**
 * I will be ensure my safety, If I'm a victim or witness to sexual harassment.
 * I will keep records of the incidents, including dates, times, locations, and any other relevant details.
 * I will contact HR or management the harassment is taking place in a workplace, report it to your Human Resources department or higher management. 
 * Most organizations have policies and procedures in place for handling harassment complaints.

