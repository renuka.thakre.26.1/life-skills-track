# GRIT:

**1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.**
* Grit is passion and perseverance for very long term goals. 
* Grit is having stamina. Grit is sticking with your future, day in, day out, not just for the week, not just for the month, but for years, 
  and working really hard to make that future a reality. 
* Grit is living life like it's a marathon, not a sprint.

# INTRODUCTION TO GROWTH MINDSET:

**2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.**
* A growth mindset can be cultivated and developed through self-awareness and intentional effort. 
* It's not about ignoring the reality of current limitations but recognizing that abilities can be developed with time and dedication. 
* This mindset can be applied in various aspects of life, including education, work, relationships, and personal development, leading to 
  greater resilience, motivation, and ultimately, higher achievement.
  
# UNDERSTANDING INTERNAL LOCUS OF CONTROL:

**3. What is the Internal Locus of Control? What is the key point in the video?**
* The locus of control is such a double-edge sword.  
* I've seen plenty of times where believing that you're in control really does help, but I've also lived through things that TRULY weren't 
  in my control, and on top of the   collateral damage from those events, I had to deal with the guilt of believing those events were 
  somehow my fault AND deal with the people around me telling me it was my fault, which lead to them not doing what THEY could control to help with the situation.  
* There's power in seizing what's in your control, but there's freedom in accepting what isn't. And there's danger in being punished for what isn't in your control.

# HOW TO BUILD A GROWTH MINDSET:

**4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).**
* Believe in your ability to thing figure out.
* Greater growth mindset?.
* Developing long term growth mindset.
* Believing in your ability to control your outcomes.

**5. What are your ideas to take action and build Growth Mindset?**
* Embrace Challenges.
* Learn Continuously.
* View Failure as Feedback.
* Develop a Debugging Mindset.
