# TINY HABITS:

**1. In this video, what was the most interesting story or idea for you?**
* Set the habit you want to make right after something you already do.
* example: Do 2 pushups right after going to the bathroom or flossing just 1 tooth after brushing your teeth. 
* What's important is to make the habit TINY so that you will do it no matter how tired or unmotivated you are, yet you will be creating the seed for a good habit nonetheless. 
(Also that you should praise yourself after doing the habit. Tell yourself "I'm awesome!" or pump your fists in the air).

**2. How can you use B = MAP to make making new habits easier? What are M, A and P.**
* According to Dr. BJ Fogg, Behavior Scientist at Stanford, B = MAP. Behavior (B) happens when Motivation (M), Ability (A), and a Prompt (P) come together at the same moment.
* Motivation is the desire to do the behavior.
* "B = MAP" is a model for habit formation:
   * Behavior (B): This is the habit you want to establish.
   * Motivation (M): Your reason for wanting to build this habit.
   * Ability (A): Making the habit easy to perform.
   * Prompt (P): Setting up reminders or triggers for the habit.
* Using this model, you can define, understand, and facilitate the process of creating and maintaining new habits.

**3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)**
* Celebrating success after completing a habit is important because it reinforces a positive association with that behavior, making it more likely to become a habit. 
* Celebrations create a sense of reward and pleasure, motivating you to repeat the action in the future. Over time, these positive associations can solidify the habit.

**4. In this video, what was the most interesting story or idea for you?**
* So about 15 years ago, early 2000s, British Cycling hires this guy named Dave Brailsford. And at that point, last like 100 years, British Cycling had been incredibly mediocre. 
* They had won a single gold medal back in 1908. They had never won the Tour de France, which is the premium race in cycling, the premier race. 
* And so they hired this guy named Dave Brailsford to change that. And in fact at the time, they were so mediocre that when they went to buy a new set of bikes, they’re getting like 200 from a top manufacturer in Europe, they actually weren’t even given quotes from the manufacturer because they didn’t want other teams to see the British riders using their gear, for fear that it would hurt sales.
* In Atomic Habits, James Clear tells us that improving by just 1% is not always noticeable but can be extremely significant in the long run. 
* The overall concept is that if you can become 1% better every day for 1 year, you'll end up 37 times better than you were at the beginning of the year.

**5. What is the book's perspective about Identity?**
* Most people build outcome-based habits instead of identity-based habits. 
* The problem here is the direction of change. We should instead focus on who we want to become instead of what superficially wants. 
* True habit change is identity change.

**6. Write about the book's perspective on how to make a habit easier to do?**
* The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become. 
* The Four Laws of Behavior Change are a simple set of rules we can use to build better habits. 
* They are:
    (1) make it obvious, 
    (2) make it attractive,
    (3) make it easy, and 
    (4) make it satisfying.

**7. Write about the book's perspective on how to make a habit harder to do?**
* If a behavior is insufficient in any of the four stages, it will not become a habit.
* Eliminate the cue and your habit will never start.
* Reduce the craving and you won't experience enough motivation to act.
* Make the behavior difficult and you won't be able to do it.

**8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**
* I want to make habit for reading book daily.
* Make the Cue Obvious: Set a specific time for read the book. This creates a clear cue for the habit.
* Make the Habit Attractive: Imagine visually in story so it becomes more attractive.
* Make it Easy: Start with manageable goals. If you're new for reading habit, don't aim for an hour-long right away. Begin with a 15-30 minute session.
* Make the Response Satisfying: Reward yourself after each session with something enjoyable. 
* Track your progress and celebrate your achievements, no matter how small. This can be satisfying and motivating.

**9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**
* I want to reduce my mobile checking habits when I'm in working hours.
* Make the Cue Invisible: Keep your mobile long distance so when you doing same it is not happen and you stop doing that.
* Make the Habit Unattractive: Do start other activity so you forget mobile thought.
* Make it Hard: If you keep your mobile where you don't want to go or not able to reach to your mobile so that your habit gets changed automatically.
* Make the Response Unsatisfying: Remove all that social media application because of that you feel happy to wont have anything to make happy.





