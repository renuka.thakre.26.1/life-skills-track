# Listening and Assertive Communication

**1. What are the steps/strategies to do Active Listening? (Minimum 6 points)**
 1. Give Your Full Attention.
 2. Maintain Eye Contact. 
 3. Use Verbal and Non-Verbal Cues.
 4. Avoid Interrupting.
 5. Paraphrase and Reflect. 
 6. Listen with emotions.
 
**2. According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English).**
 1. Hearing and understanding what the other person is communicating through words and “body language” to the best of your ability. 
 2. Responding to the other person by reflecting the thoughts and feelings you heard in his or her words. 
 3. Tone of voice, body posture, and gestures. you are listening with precision.

**3. What are the obstacles in your listening process?**
 1. Physical and Environmental Barriers.
 2. Cultural Barriers.
 3. Emotional and Psychological Barriers.
 4. Physiological Barriers.
 5. Language Barriers.
 6. Making Assumptions.
 7. Too Much Information.
 8. Tone of Voice.
      
**4. What can you do to improve your listening?**
 1. I try to make Face the speaker and have eye contact, never interrupt and have some related expression as well.
 
 
**5. When do you switch to Passive communication style in your day to day life?**
 1. The passive communication style is often used by those who want to come off as indifferent about the topic at hand. 
 2. They either keep their opinions to themselves or try to make it seem as if they support every piece of input in the discussion.
 
**6. When do you switch into Aggressive communication styles in your day to day life?**
 1. I only engage in an aggressive communication style typically in situations where I need to assert myself or protect my boundaries. 
 2. This might happen if I perceive a threat, encounter a situation where assertiveness is vital.
 3. I prefer to use a more balanced and assertive communication style that promotes healthy dialogue and cooperation.
 
**7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**
 1. I generally try to avoid passive-aggressive communication styles in my day-to-day interactions.
 2. Instead, I aim to express my concerns, frustrations, or opinions directly and assertively when needed.
 
 **8. How can you make your communication assertive? You can watch and analyses the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)**
 1. Self awareness.
 2. Set boundaries.
 3. Stay clam.
 4. Practice empathy.
 5. Use Assertive Body Language.
 
