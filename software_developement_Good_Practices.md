# Good Practices for Software Development

**1. Which point(s) were new to you?**
* Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup.
* You should strongly consider blocking social media sites and apps during work hours. Tools like TimeLimit, Freedom, or any other app can be helpful.

**2. Which area do you think you need to improve on? What are your ideas to make progress in that area?**
* Doing things with 100% involvement.
* Make sure you manage your food situation well. Too little food or too much food lead to lower levels of concentration.
* Make sure you do some exercise to keep your energy levels high throughout the day.
