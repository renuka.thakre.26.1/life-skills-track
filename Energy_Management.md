**1. What are the activities you do that make you relax - Calm quadrant?**
* Listening music
* Talking with friends
* Going to take sleep.

**2. When do you find getting into the Stress quadrant?**
* Feeling under pressure to do work fast that I don't have hands-on practice.
* When anyone going to oppose my thoughts that I confirm on that.
* If my didn't manage proper time or any followups.

**3. How do you understand if you are in the Excitement quadrant?**
* If any work I done on time that I don't expect from me.
* Whenever I learn new things and understanding concepts clearly.

**4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.**
* sleep for learning pre and post.
* hippocampus.
* short long term memory.

**5. What are some ideas that you can implement to sleep better?**
* Maintain a Consistent Sleep Schedule.
* Create a Comfortable Sleep Environment.
* Limit Screen Time Before Bed.
* Mind my diet.

**6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.**
* Physical activity has a profound impact on the brain.
* Exercise can boost memory and learning by promoting the release of neurotransmitters like dopamine and norepinephrine.
* The brain's ability to rewire itself and adapt, which is fundamental for learning and memory.
* Do aerobic exercise, such as running or cycling.
* Exercise contributes to overall well-being by reducing stress, anxiety, and depression.

**7. What are some steps you can take to exercise more?**
* Set Clear Goals.
* Start Small.
* Choose Activities You Enjoy.
* Find a Workout Buddy.
* Incorporate Movement into Daily Life.
* Variety Is Key.
* Set Realistic Expectations.
* Reward Yourself.
* Stay Accountable
* Prioritize Consistency.
* Listen to Your Body.
* Make it Enjoyable.






