# Learning Process

**1. What is the Feynman Technique? Explain in 1 line?**
 1. Note down in simple English word and try to explain anyone.

**2. In this video, what was the most interesting story or idea for you?**
 1. The most interesting story was for me one painter guy is doing painting and when he is stuck in creating or making ideas he was just relax on seat, take key and continuously revolving once he get sleep that key goes down and that sound make he wake up so he goes to relax mind to focus mind that is really interesting story and technique as well. 
 2. Also, teaching yourself is very good part of learn easily so got concept clear again not only you have to clear concepts you to do exercises and test continuously to thinking what we learn and what we able to do. 

**3. What are active and diffused modes of thinking?**
 1. Active modes or focus mode is when we actively doing our work but when we get stuck we allow to go in diffused mode so the diffused mode is you turn from your problem to other  work so your mind will start about that problem freshly.

**4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.**
 1. Deconstruct the skill into small parts.
 2. start understanding topic and teach yourself.
 3. Remove barrier practice.
 4. Practice at least 20 hours.


**5. What are some of the actions you can take going forward to improve your learning process?**
 1. understand concept very clearly
 2. try to explain yourself
 3. practice more.

